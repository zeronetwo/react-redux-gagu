import React from 'react'
import Users from './Users'
import { connect } from 'react-redux'

class App extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      info: {
        name: '',
        username: '',
        email: ''
      },
      button_label: 'Add User'
    }
  }

  componentDidMount() {
    this.props.getUsers()
  }

  handeInpuChange = (e) => {
    const { value, id } = e.target
    const { info } = this.state

    this.setState({
      info: {
        ...info,
        [id]: value
      }
    })
  }

  handleAddUser = () => {
    const { addUser } = this.props
    const { info: { name, username, email } } = this.state
    if (!name && !username && !email) {
      return 
    }
  
    addUser(this.state.info)

    this.setState({
      info: {
        name: '',
        username: '',
        email: ''
      }
    })
  }

  handleEdit = (user) => {
    this.setState({
      info: user,
      button_label: 'Save User'
    })
  }

  handeSaveUser = () => {
    this.props.editUser(this.state.info)

    this.setState({
      info: {
        name: '',
        username: '',
        email: ''
      },
      button_label: 'Add User'
    })
  }

  render() {
    const { info, button_label } = this.state
    const { name, username, email } = info

    console.log('@props', this.props)
    return (
      <div>
        <div style={{ position: 'fixed', top: '200px', right: '200px', width: '300px' }}>
          <div>
            <input
              style={{ padding: '10px', width: '100%' }}
              placeholder='Name'
              value={name}
              id='name'
              onChange={this.handeInpuChange}
            />
          </div>
          <div>
            <input
              style={{ padding: '10px', width: '100%' }}
              placeholder='Username'
              value={username}
              id='username'
              onChange={this.handeInpuChange}
            />
          </div>
          <div>
            <input
              style={{ padding: '10px', width: '100%' }}
              placeholder='Email'
              value={email}
              id='email'
              onChange={this.handeInpuChange}
            />
          </div>
          <div>
            <button
              style={{ padding: '10px', width: '108%' }}
              onClick={
                button_label === 'Add User'
                ? this.handleAddUser
                : this.handeSaveUser
              }
            >{button_label}</button>
          </div>
        </div>
        <Users handleEdit={this.handleEdit} />
      </div>
    )
  }
}

const MapStateToProps = (state) => {
  return {
    users: state.users,
    companes: state.companies
  }
}

const MapActionsToProps = (dispatch) => {
  return {
    addUser: (payload) => dispatch({
      type: 'ADD_USER',
      payload
    }),
    editUser: (payload) => dispatch({
      type: 'EDIT_USER',
      payload
    }),
    getUsers: () => dispatch({
      type: 'GET_USERS_REQUESTED'
    })
  }
}

export default connect(MapStateToProps, MapActionsToProps)(App)
