import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import { Provider } from 'react-redux'
import { createStore, combineReducers, applyMiddleware } from 'redux'
import createSagaMiddleware from 'redux-saga'

import UserReducer from './reducers/UserReducer'
import CompanyReducer from './reducers/CompanyReducer'
import UserSaga from './sagas/UserSaga'

const reducers = combineReducers({
  users: UserReducer,
  companies: CompanyReducer
})

const INITIAL_STATE = {
  users: [],
  companies: []
}

const sagaMiddleware = createSagaMiddleware()

const store = createStore(
  reducers,
  INITIAL_STATE,
  applyMiddleware(sagaMiddleware)
)

sagaMiddleware.run(UserSaga)

ReactDOM.render(
  <Provider store={store}>
    <App />
  </Provider>,
  document.getElementById('root')
)