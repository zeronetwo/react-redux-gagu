import { call, put, takeEvery } from 'redux-saga/effects'
import { uuid } from 'uuidv4'

async function fetchJson(url) {
  let resp;
  try {
      let data = await fetch(url);
      resp = { data: await data.json() };
  }
  catch (e) {
      resp = { err: e.message };
  }
  return resp;
}

function* fetchUser(action) {
  console.log('SAGA action', action)
   try {
      const users = yield call(fetchJson, 'https://jsonplaceholder.typicode.com/users')
      yield put({
        type: "GET_USERS",
        payload: users.data.map(({ name, username, email }) => ({ id: uuid(), name, username, email }))
      })
   } catch (e) {
      yield put({ type: "GET_USERS_FAILED", message: e.message });
   }
}

function* UserSaga() {
  yield takeEvery('GET_USERS_REQUESTED', fetchUser);
}

export default UserSaga