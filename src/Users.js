import React from 'react'
import { connect } from 'react-redux'

const Users = (props) => {
  console.log('@USERS', props)
  const { users, deleteUser, handleEdit } = props
  return (
    <div>
      <h3>Users</h3>
      {
        users.map((e, i) => {
          const { id, name, username, email } = e
          return (
            <div key={i} style={{ padding: '10px' }}>
              <div>Name: <strong>{name}</strong> </div> 
              <div>Username: <strong>{username}</strong> </div> 
              <div>Email: <strong>{email}</strong></div>
              <button style={{ marginRight: '5px' }}
                onClick={() => deleteUser(id)}
              >Delete</button>
              <button
                onClick={() => handleEdit(e)}
              >Edit</button>
            </div>
          ) 
        })
      }
    </div>
  )
}

const MapStateToProps = ({ users }) => ({ users })
const MapActionsToProps = (dispatch) => {
  return {
    deleteUser: (payload) => dispatch({
      type: 'DELETE_USER',
      payload
    })
  }
}

export default connect(MapStateToProps, MapActionsToProps)(Users)