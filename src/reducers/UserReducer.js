import { uuid } from 'uuidv4'

const UserReducer = (state = [], { type, payload }) => {
  console.log('@USERREDUCER', { state, type, payload })
  if (type === 'ADD_USER') {
    const userExist = state.find(({ email }) => email === payload.email)
    return userExist ? state : [...state, { ...payload, id: uuid() }]
  }
  if (type === 'DELETE_USER') {
    const new_state = [...state].filter(({ id }) => id !== payload)
    return new_state
  }
  if (type === 'EDIT_USER') {
    console.log('zzzzz', payload)
    const new_state = [...state].map(e => {
      return e.id === payload.id ? payload : e
    })
    return new_state
  }
  if (type === 'GET_USERS') {
    return payload
  }
  return state
}

export default UserReducer